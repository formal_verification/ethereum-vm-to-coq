let coqInductiveType = "
Inductive opcodes : Set :=
| Stop : opcodes
| Add : opcodes
| Mul : opcodes
| Sub : opcodes
| Div : opcodes
| Sdiv : opcodes
| Mod : opcodes
| Smod : opcodes
| Addmod : opcodes
| Mulmod : opcodes
| Exp : opcodes
| Signextend : opcodes
| Lt : opcodes
| Gt : opcodes
| Slt : opcodes
| Sgt : opcodes
| Eq : opcodes
| Iszero : opcodes
| And : opcodes
| Or : opcodes
| Xor : opcodes
| Not : opcodes
| Byte : opcodes
| Shl : opcodes
| Shr : opcodes
| Sar : opcodes
| Keccak256 : opcodes
| Address : opcodes
| Balance : opcodes
| Origin : opcodes
| Caller : opcodes
| Callvalue : opcodes
| Calldataload : opcodes
| Calldatasize : opcodes
| Calldatacopy : opcodes
| Codesize : opcodes
| Codecopy : opcodes
| Gasprice : opcodes
| Extcodesize : opcodes
| Extcodecopy : opcodes
| Returndatasize : opcodes
| Returndatacopy : opcodes
| Extcodehash : opcodes
| Blockhash : opcodes
| Coinbase : opcodes
| Timestamp : opcodes
| Number : opcodes
| Difficulty : opcodes
| Gaslimit : opcodes
| Pop : opcodes
| Mload : opcodes
| Mstore : opcodes
| Mstore8 : opcodes
| Sload : opcodes
| Sstore : opcodes
| Jump : opcodes
| Jumpi : opcodes
| Pc : opcodes
| Msize : opcodes
| Gas : opcodes
| Jumpdest : opcodes
| Push1 : string -> opcodes
| Push2 : string -> opcodes
| Push3 : string -> opcodes
| Push4 : string -> opcodes
| Push5 : string -> opcodes
| Push6 : string -> opcodes
| Push7 : string -> opcodes
| Push8 : string -> opcodes
| Push9 : string -> opcodes
| Push10 : string -> opcodes
| Push11 : string -> opcodes
| Push12 : string -> opcodes
| Push13 : string -> opcodes
| Push14 : string -> opcodes
| Push15 : string -> opcodes
| Push16 : string -> opcodes
| Push17 : string -> opcodes
| Push18 : string -> opcodes
| Push19 : string -> opcodes
| Push20 : string -> opcodes
| Push21 : string -> opcodes
| Push22 : string -> opcodes
| Push23 : string -> opcodes
| Push24 : string -> opcodes
| Push25 : string -> opcodes
| Push26 : string -> opcodes
| Push27 : string -> opcodes
| Push28 : string -> opcodes
| Push29 : string -> opcodes
| Push30 : string -> opcodes
| Push31 : string -> opcodes
| Push32 : string -> opcodes
| Dup1 : opcodes
| Dup2 : opcodes
| Dup3 : opcodes
| Dup4 : opcodes
| Dup5 : opcodes
| Dup6 : opcodes
| Dup7 : opcodes
| Dup8 : opcodes
| Dup9 : opcodes
| Dup10 : opcodes
| Dup11 : opcodes
| Dup12 : opcodes
| Dup13 : opcodes
| Dup14 : opcodes
| Dup15 : opcodes
| Dup16 : opcodes
| Swap1 : opcodes
| Swap2 : opcodes
| Swap3 : opcodes
| Swap4 : opcodes
| Swap5 : opcodes
| Swap6 : opcodes
| Swap7 : opcodes
| Swap8 : opcodes
| Swap9 : opcodes
| Swap10 : opcodes
| Swap11 : opcodes
| Swap12 : opcodes
| Swap13 : opcodes
| Swap14 : opcodes
| Swap15 : opcodes
| Swap16 : opcodes
| Log0 : opcodes
| Log1 : opcodes
| Log2 : opcodes
| Log3 : opcodes
| Log4 : opcodes
| Create : opcodes
| Call : opcodes
| Callcode : opcodes
| Return : opcodes
| Delegatecall : opcodes
| Create2 : opcodes
| Staticcall : opcodes
| Revert : opcodes
| Invalid : opcodes
| Selfdestruct : opcodes
| Unknown : string -> opcodes.
"
