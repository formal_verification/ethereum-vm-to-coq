# Ethereum VM to Coq

A tool for converting EVM bytecode into Coq or into the list of opcodes.
Bytecode of the smart-contract can be taken when smart-contract is deployed.

## Description 

In this project we created an AST of EVM on OCaml. We wrote a
parser (on OCaml) which parses EVM code. Many functions for the parser were taken from the [EVM-analyzer project](https://github.com/danhper/evm-analyzer/).
We created Inductive datatype in Coq for opcodes. 
Opcodes, parsed from the bytecode can be converted into Coq or into the list of opcodes.

## Usage

```
git clone https://gitlab.com/formal-land/ethereum-vm-to-coq.git
```
- place your smart_contracts bytecode into the file INIT_bytecode.txt
- open file bin/main.ml and edit it according to your needs : one out of next two functions should be commented out 
  ```
  let () = OpcodeParser.convert_to_EVM_opcodes ()
  let () = OpcodeParser.convert_to_coq ()
  ```
  
- run in command line
```
dune exec evm_coq
```
- get result code in the file RESULT_coq.txt

## License
Project is opened, anyone can copy and use it.

## Project status
This project is under development. We are planning to add more features.
